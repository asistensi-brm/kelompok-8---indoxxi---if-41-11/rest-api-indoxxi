# Indo Api Backend


## What is an API
API is the acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. Each time you use an app like Facebook, send an instant message, or check the weather on your phone, you’re using an API.

## Summary
Frima is task from our lecture to make or build our own website, frima using
API system like other API, but because is for a web i just make post and get

## Usage
Api is easy to use, you can search on the google "<your framework> get http request" or something like that
for example "ajax get http request" >> ["How Get Http Request From Ajax"](https://www.w3schools.com/jquery/jquery_ajax_get_post.asp)

## Testing
There are many way to test api, the simplest way is just browse the link, lets try this
```
http://localhost:3000/
```
but if you just browse, you cant test other function like POST

so if you want to try all of this API functionality, use postman apps, you can download it below
["Download Postman"](https://www.getpostman.com/downloads/)

## Ready API
Ready HTTP request for this Api was listed below

### is user login[POST]
Create new user for login website

Request need
```
URL : http://localhost:3000/isLogin
```
```
Body: {
    "token": "user token here"
}
```
Response result
```
{
    isLoggedin : boolean,
	uid : int
}
```

### Search Film[POST]
Search for Film

Request need
```
URL : http://localhost:3000/searchFilm
```
```
Body: { "name": "", "nation": "USA", "genre": "", "orderby": "terbaru", "uid": -1 (optional just for whistlist),"page": 1 }
```
Response result
```
{
    data : query result,
	count : int (page count)
}
```

### Get Random Film[GET]
Get Random Film 5

Request need
```
URL : http://localhost:3000/getFilm/random
```
```
Body: -
```
Response result
```
[
    query result
]
```

### Get Film Whistlist[GET]
Get Film from user whistlist

Request need
```
URL : http://localhost:3000/getFilm/whistlist/:uid/:page
```
```
Body: -
```
Response result
```
[
    query result,
]
```

### Get Film Populer[GET]
Get Film from populer

Request need
```
URL : http://localhost:3000/getFilm/populer/:page
```
```
Body: -
```
Response result
```
[
    query result,
]
```

### Get Film Terbaru[GET]
Get Film from terbaru

Request need
```
URL : http://localhost:3000/getFilm/terbaru/:page
```
```
Body: -
```
Response result
```
[
    query result,
]
```


## Test API
Just test HTTP request for this Api was listed below

### Test001 [GET]
Request need
```
URL : http://localhost:3000/
```
Response result
```
[{
    //All user Data
}]
```

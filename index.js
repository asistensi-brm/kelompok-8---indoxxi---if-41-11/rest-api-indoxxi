const express = require('express')
const app = express()
const port = 3000

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json())
app.use(express.static('public'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// Get the mysql service
const mysql = require('mysql');


// Add the credentials to access your database
const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'indoxxi'
});

// connect to mysql
connection.connect(function(err) {
	// in case of error
	if(err){
		console.log(err.code);
		console.log(err.fatal);
	}
});


var request = require("request");
var cheerio = require("cheerio");



app.get('/', (req, res) => {
	
	// Perform a query
	console.log('hellow world');
	$query = "SELECT * from user;";
	
	
	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}
		res.send(rows);
	});
	

})
app.get('/getFilm/terbaru/:page', (req, res) => {
	
	// Perform a query
	$query = "SELECT * FROM film ORDER BY upload_time DESC LIMIT 24";
	if(req.params.page != null){
		
		$query += " OFFSET "+((parseInt(req.params.page)-1)*24);
	}
	console.log($query);
	
	
	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}
		res.send(rows);
	});
	
})
app.get('/getFilm/populer/:page', (req, res) => {
	
	// Perform a query
	$query = "SELECT * FROM film ORDER BY trending LIMIT 24";
	if(req.params.page != null){
		
		$query += " OFFSET "+((parseInt(req.params.page)-1)*24);
	}
	console.log($query);
	
	
	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}
		res.send(rows);
	});
	
})
app.get('/getFilm/whistlist/:uid/:page', (req, res) => {
	
	// Perform a query
	$query = "SELECT * FROM film inner join (SELECT * FROM whistlist WHERE uid="+req.params.uid+") as us ON film.id=us.fid ORDER BY datetime DESC LIMIT 24";
	if(req.params.page != null){
		
		$query += " OFFSET "+((parseInt(req.params.page)-1)*24);
	}
	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}
		res.send(rows);
		
	});
	
})
app.get('/getFilm/random', (req, res) => {
	
	// Perform a query
	$query = "SELECT * FROM film ORDER BY RAND() LIMIT 5";
	
	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}
		res.send(rows);
		
	});
})

app.post('/searchFilm', (req, res) => {
	
	
	if(req.body.orderby == "whistlist" && req.body.uid != null && req.body.uid!= -1){
		$query = "SELECT * FROM film inner join (SELECT * FROM whistlist WHERE uid="+req.body.uid+") as us ON film.id=us.fid";
		$query2 = "SELECT CEIL(count(*)/24) as count FROM film inner join (SELECT * FROM whistlist WHERE uid="+req.body.uid+") as us ON film.id=us.fid";
	}else{
		// Perform a query
		$query2 = "SELECT CEIL(count(*)/24) as count FROM film";
		$query = "SELECT * FROM film";
	}
	let i = 0;
	let sqlsub = [];
	if(req.body.nation != ""){
		
		let str = req.body.nation;
		let split = str.split(",");
		let sqlWhere = "";
		for(let i =0 ; i < split.length;i++){
			sqlWhere += " nation LIKE '%"+split[i]+"%'"
			if(i!= split.length-1){
				sqlWhere += " OR";
			}
		}
		sqlsub[i] = sqlWhere;
		i++;
	}
	if(req.body.genre != ""){
		
		let str = req.body.genre;
		let split = str.split(",");
		let sqlWhere = "";
		for(let i =0 ; i < split.length;i++){
			sqlWhere += " genre LIKE '%"+split[i]+"%'"
			if(i!= split.length-1){
				sqlWhere += " OR";
			}
		}
		sqlsub[i] = sqlWhere;
		i++;
		
	}
	if(req.body.name != ""){
		
		let sqlWhere = "";
		
		sqlWhere = " judul LIKE '%"+req.body.name+"%'";
		
		sqlsub[i] = sqlWhere;
		i++;
		
	}
	if(i>0){
		$query2 += " WHERE";
		$query += " WHERE";
	}
	$query += sqlsub.join(" AND");
	
	$query2 += sqlsub.join(" AND");
	
	if(req.body.orderby == "terbaru"){
		$query += " ORDER BY upload_time DESC LIMIT 24";
	}else if(req.body.orderby == "populer"){
		$query += " ORDER BY trending LIMIT 24";
	}else if(req.body.orderby == "rating"){
		$query += " ORDER BY rating DESC LIMIT 24";
	}else{
		$query += " ORDER BY upload_time DESC LIMIT 24";
	}
	
	$query += " OFFSET "+((parseInt(req.body.page)-1)*24);

	console.log($query2);

	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}
		connection.query($query2, function(err2, rows2, fields2) {
			if(err2){
				console.log("An error ocurred performing the query.");
				return;
			}

			res.send({data : rows , count : Math.max(1, rows2[0].count) });
		});

		//res.send(rows);
	});

})



app.post('/isLogin', (req, res) => {
	
	// Perform a query
	$query = "SELECT * from user where token='"+req.body.token+"';";

	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			return;
		}

		console.log("Query succesfully executed: ", rows.length);
		if(rows.length==1){
			res.send({isLoggedin : true , id : rows[0].id ,data : rows[0]});
		}else{
			res.send({isLoggedin : false , id : -1,data : null});
		}
	});

})

app.post('/addToWishlist', (req, res) => {
	
	// Perform a query
	$query = "INSERT INTO whistlist (`comb`, `uid`, `fid`, `datetime`) VALUES ('"+req.body.uid+","+req.body.fid+"', '"+req.body.uid+"', '"+req.body.fid+"', NOW());";

	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			res.send({success : false});
			return;
		}
		res.send({success : true});
	});

})
app.post('/removeFromWishlist', (req, res) => {
	
	// Perform a query
	$query = "DELETE FROM whistlist WHERE uid='"+req.body.uid+"' AND fid='"+req.body.fid+"' AND comb='"+req.body.uid+","+req.body.fid+"'";

	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			res.send({success : false});
			return;
		}
		res.send({success : true});
	});

})
app.post('/isInWishlist', (req, res) => {
	
	// Perform a query
	$query = "SELECT * from whistlist WHERE uid='"+req.body.uid+"' AND fid='"+req.body.fid+"' AND comb='"+req.body.uid+","+req.body.fid+"'";
	
	connection.query($query, function(err, rows, fields) {
		if(err){
			console.log("An error ocurred performing the query.");
			res.send({success : false});
			return;
		}
		res.send({success : (rows.length==1)});
	});

})
// Close the connection
//connection.end(function(){
	// The connection has been closed
//});
app.listen(port, () => console.log(`Example app listening on port ${port}!`))